package usthon.syafii.appx07

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnScanQR -> {
                //initiate a barcode scans and plays 'beep' sound when a barcode is detected
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR -> {
                //initiate barcode encoder
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400,400)
                //display QR Code image using ImageView
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSimpan -> {
                builder.setTitle("Alert").setMessage("Apakah data ingin disimpan?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            //set edQrCode with content from QR Code Scanner
            edQrCode.setText(intentResult.contents)
            //set tokenizer to break edQrCode
            val strToken = StringTokenizer (edQrCode.text.toString(),";",false)
            edNim.setText(strToken.nextToken())
            edNamaMhs.setText(strToken.nextToken())
            edProdi.setText(strToken.nextToken())
        }else{
            Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var builder: AlertDialog.Builder

    //IntentIntegrator is a part of zxing-android-embedded library that is used to read QR Code
    lateinit var intentIntegrator: IntentIntegrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)

        //initialize 'save to database'
        db = DBOpenHelper(this).writableDatabase
        builder = AlertDialog.Builder(this)
        btnSimpan.setOnClickListener(this)
    }


    override fun onStart() {
        super.onStart()
        showDataMhs()
    }

    fun showDataMhs(){
        val cursor : Cursor = db.query("mhs", arrayOf("nim as _id","nama","prodi"),
            null,null,null,null,"nim asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_data_mhs,cursor,
            arrayOf("_id","nama","prodi"), intArrayOf(R.id.txNim,R.id.txNamaMhs,R.id.txProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMhs.adapter = adapter
    }

    //save to database
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(
            edNim.text.toString(),
            edNamaMhs.text.toString(),
            edProdi.text.toString()
        )
        edNim.setText("")
        edNamaMhs.setText("")
        edProdi.setText("")
    }

    fun insertDataMhs(nim:String, nama:String, prodi:String){
        var cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        cv.put("nama",nama)
        cv.put("prodi",prodi)
        db.insert("mhs",null,cv)
        showDataMhs()
        Toast.makeText(this,"data berhasil disimpan",Toast.LENGTH_SHORT).show()
    }


}